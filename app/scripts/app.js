'use strict';

var App = angular
  .module('nurseLolaAngularApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'braintree-angular',
    'ui.bootstrap',
    'angularFileUpload'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainController'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutController'
      })
      .when('/users/new', {
        templateUrl: 'views/users/new.html',
        controller: 'RegistrationsController'
      })
      .when('/dashboard', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardController'
      })
      .when('/forgot_password', {
        templateUrl: 'views/forgot/password.html',
        controller: 'ForgotController'
      })
      .when('/forgot_password_reset', {
        templateUrl: 'views/forgot/password_reset.html',
        controller: 'ForgotController'
      })
      .when('/forgot_username', {
        templateUrl: 'views/forgot/username.html',
        controller: 'ForgotController'
      })
      .when('/forgot_username_reset', {
        templateUrl: 'views/forgot/username_reset.html',
        controller: 'ForgotController'
      })
      .when('/sessions/new', {
        templateUrl: 'views/sessions/new.html',
        controller: 'SessionsController'
      })
      .when('/organizations/:organizationId/employees/new', {
        templateUrl: 'views/employees/new.html',
        controller: 'EmployeesController'
      })
      .when('/users/edit', {
        templateUrl: 'views/users/edit.html',
        controller: 'UsersController'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
