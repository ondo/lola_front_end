'use strict';

App.service('Registration', ['$cookies', '$http', 'Server', function ($cookies, $http, Server) {
    this.create = function (err, nonce, user, organization, plan) {
      var params = {
        plan: plan,
        user: {
          first_name: user.firstName,
          last_name: user.lastName,
          username: user.username,
          phone_number: user.phoneNumber,
          email: user.email,
          password: user.password,
          password_confirmation: user.passwordConfirmation
        },
        organization_name: organization.name
      };
      if (nonce) {
        params.payment_method_nonce = nonce;
      }
      return $http.post(Server + '/users.json', params);
    };
  }]);
