'use strict';

App.service('Session', ['$cookies', function ($cookies) {
    this.create = function (authToken, userId) {
      this.authToken = authToken;
      this.id = userId;
      $cookies._nurseLola = authToken;
    };
    this.destroy = function () {
      this.authToken = null;
      this.id = null;
      delete $cookies._nurseLola;
    };
  }]);