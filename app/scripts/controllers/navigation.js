'use strict';

App.controller('NavigationController', ['$scope', 'AUTH_EVENTS',
  function ($scope, AUTH_EVENTS) {
    var loggedInTemplate = 'views/loggedInNavigation.html';
    var loggedOutTemplate = 'views/loggedOutNavigation.html';

    $scope.navigationTemplateUrl = $scope.currentUser ? loggedInTemplate : loggedOutTemplate;

    $scope.$on(AUTH_EVENTS.loginFailed, function () {
      $scope.navigationTemplateUrl = loggedOutTemplate;
    });

    $scope.$on(AUTH_EVENTS.logoutSuccess, function () {
      $scope.navigationTemplateUrl = loggedOutTemplate;
    });

    $scope.$on(AUTH_EVENTS.loginSuccess, function () {
      $scope.navigationTemplateUrl = loggedInTemplate;
    });
    // (function SeachForm() {
    //   var animationTime = 700;
    //   var fontSearchOpenPosition = 'font-search-open-position';
    //   var fontAnimatingPosition = 'font-animating-position';

    //   function animation (toggler) {
    //     var inputBoxWidth, widthSign, widthDelta;

    //     inputBoxWidth = '=190';
        
    //     widthSign = {
    //       show: '+',
    //       hide: '-'
    //     };

    //     widthDelta = widthSign[toggler] + inputBoxWidth;

    //     return {
    //       width: widthDelta
    //     };
    //   }
      
    //   function toggleForm (form, toggler, completion) {
    //     form.animate(animation(toggler), animationTime, completion);
    //   }

    //   function hideForm (form) {
    //     // var navigationFonts = angular.element('.left-nav-font');

    //     // navigationFonts.removeClass(fontSearchOpenPosition);
    //     // navigationFonts.addClass(fontAnimatingPosition);


    //     toggleForm(form, 'hide', function(){
    //       form.hide();
    //       // navigationFonts.removeClass(fontAnimatingPosition);
    //     });
    //   }
      
    //   function showForm (form) {
    //     // var navigationFonts = angular.element('.left-nav-font');

    //     // navigationFonts.addClass(fontAnimatingPosition);

    //     form.css('display', 'inline-block');
    //     toggleForm(form, 'show', function(){
    //       form.children('input').focus();
    //       // navigationFonts.addClass(fontSearchOpenPosition);
    //       // navigationFonts.removeClass(fontAnimatingPosition);
    //     });
    //   }

    $scope.searchClick = function () {
      var form = angular.element('#search-form');
      form.children('input').focus();

      // $scope.searchDisplay = !$scope.searchDisplay;

      // if ($scope.searchDisplay) {
        
      //   showForm(form);
      // } else {
      //   hideForm(form);
      // }
    };

    //   // $scope.blurSearchForm = function() {
    //   //   var form = angular.element('#search-form');
    //   //   hideForm(form);
    //   //   $scope.searchDisplay = true;
    //   // };
      
    // })();


    (function Sidebar () {

      var animationTime = 400;

      function animateSidebarOpen (sidebarElement, property) {
        var animation = {};
        animation[property] = '+=100';
        sidebarElement.animate(animation, animationTime, function(){});
      }

      function animateSidebarClose (sidebarElement, property) {
        var animation = {};
        animation[property] = '-=100';
        sidebarElement.animate(animation, animationTime, function(){
            var sidebarExtra = angular.element('.sidebar-extra');
            sidebarExtra.hide();
          }
        );
      }


      $scope.expandSidebar = function () {
        $scope.sidebarNotExpanded = !$scope.sidebarNotExpanded;
        var sidebarExtra = angular.element('.sidebar-extra');
        var sidebar = angular.element('.sidebar');
        var searchBar = angular.element('.search-bar');
        
        if ($scope.sidebarNotExpanded) {
          sidebarExtra.show();
          animateSidebarOpen(sidebarExtra, 'width');
          animateSidebarOpen(sidebar, 'width');
          animateSidebarOpen(searchBar, 'margin-left');
        } else {
          animateSidebarClose(sidebarExtra, 'width');
          animateSidebarClose(sidebar, 'width');
          animateSidebarClose(searchBar, 'margin-left');
        }
      };
    })();
  }]);
