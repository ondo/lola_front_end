'use strict';

App.controller('ForgotController', ['$scope', '$http', 'Server', '$routeParams', '$location', '$rootScope',
  function ($scope, $http, Server, $routeParams, $location, $rootScope){

    $scope.forgotPassword = function (user) {
      $http.post(Server + '/forgot_password', {
        user: {
          username: user.username
        }
      })
      .success(function (data) {
        $scope.emailSent = true
      });
    };

    function passwordResetError (user) {
      if (passwordDoesNotMatchConfirmation(user)) {
        $scope.errorMessage = 'The password you entered does not match the password confirmation.'
      }
    }

    function passwordDoesNotMatchConfirmation (user) {
      return user.password !== user.passwordConfirmation;
    }

    $scope.forgotPasswordReset = function (user) {
      if (!user.password || !user.passwordConfirmation || passwordDoesNotMatchConfirmation(user)) {
        passwordResetError(user);
      } else {
        $http.post(Server + '/forgot_password_reset', {
          reset_password_token: $routeParams.reset_password_token,
          user: {
            password: user.password,
            password_confirmation: user.passwordConfirmation
          }
        })
        .success(function (data){

        });
      }
    };

    $scope.forgotUsername = function (user) {
      $http.post(Server + '/forgot_username', {
        user: {
          email: user.email
        }
      })
      .success(function (data) {
        $scope.emailSent = true;
      })
    };

    $scope.forgotUsernameReset = function (user) {
      $http.post(Server + '/forgot_username_reset', {
        user: {
          username: user.username
        }
      })
      .success(function (data) {
        
      });
    }
  }]);