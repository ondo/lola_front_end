'use strict';

App.controller('UsersController', ['$scope', '$upload', 'Server', '$cookies', '$rootScope',
  function ($scope, $upload, Server, $cookies, $rootScope) {
    $scope.$watch('files', function () {
      $scope.upload($scope.files);
    });

    $scope.upload = function (files) {
      if (files && files.length) {
        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          $upload.upload({
            url: Server + '/user_avatar',
            method: 'PATCH',
            fields: {
              'auth_token': $cookies._nurseLola
            },
            file: file
          }).progress(function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total, 10);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
          }).success(function (data, status, headers, config) {
            $rootScope.setProfilePicture(data.avatar);
            console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
          });
        }
      }
    };
  }]);