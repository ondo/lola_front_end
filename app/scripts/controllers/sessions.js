'use strict';

App.controller('SessionsController', ['$scope', '$modal', '$rootScope', 'AuthService', 'AUTH_EVENTS', '$location',
    function ($scope, $modal, $rootScope, AuthService, AUTH_EVENTS, $location) {

    $scope.openLoginModal = function(){
      if (angular.element(window).width() > 768) {
        $modal.open({
          templateUrl: 'views/sessions/loginModal.html',
          controller: 'LoginsController',
          size: 'sm'
        });
      } else {
        $location.path('/sessions/new');
      }
    };

    $scope.logout = function() {
      AuthService.logout();
      $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
    };

    $scope.login = function (credentials) {
      AuthService.login(credentials).success(function (data) {
        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
        $rootScope.setCurrentUserData(data);
        $location.path('/');
      }).error(function (data) {
        $rootScope.loginError = 'Invalid Username or Password';
        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
      });
    };
  }]);
