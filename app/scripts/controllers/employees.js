'use strict';

App.controller('EmployeesController', ['$scope', '$cookies', '$http', 'Server', '$routeParams',
  function ($scope, $cookies, $http, Server, $routeParams) {

  $scope.addEmployee = function (employee){
    $http.post(Server + '/organizations/' + $routeParams.organizationId + '/invitaions', {
      auth_token: $cookies._nurseLola,
      invite: {
        email: employee.email,
        role: employee.role
      }
    })
    .success(function (data){
      $scope.invite = data.invite;
    })
    .error(function(){
      $scope.errorMessage = 'Something Went Wrong!';
    });
  };
}]);