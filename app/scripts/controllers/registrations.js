'use strict';

App.controller('RegistrationsController', ['$scope', '$braintree', '$routeParams', '$location', '$rootScope', '$cookies', 'Registration', 'AUTH_EVENTS',
    function ($scope, $braintree, $routeParams, $location, $rootScope, $cookies, Registration, AUTH_EVENTS) {
    if (!$routeParams.plan || $routeParams.plan === 'individual') {
      $scope.plan = false;
    } else {
      $scope.plan = $routeParams.plan;
    }

    var braintree = function() {
      $braintree.getClientToken().success(function(data) {
        $scope.client = new $braintree.api.Client({
          clientToken: data.clientToken
        });
      });
    };

    $scope.creditCard = {};

    function registrationSuccess (err, nonce, user, organization, planName) {
      return Registration.create(err, nonce, user, organization, planName)
        .success(function(data){
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
            $cookies._nurseLola = data.user.auth_token;
            $rootScope.setCurrentUserData(data);
          });
    }

    $scope.sendForm = function(){
      if ($routeParams.plan === 'team' || $routeParams.plan === 'enterprise') {
        $scope.client.tokenizeCard({
          number: $scope.creditCard.number,
          cardholderName: $scope.user.firstName + ' ' + $scope.user.lastName,
          expirationDate: $scope.creditCard.expirationDate,
          cvv: $scope.creditCard.cvv
        }, function (err, nonce) {
          registrationSuccess(err, nonce, $scope.user, $scope.organization, $routeParams.plan);
        });
      } else {
        registrationSuccess(null, null, $scope.user, $scope.organization, $routeParams.plan);
      }
    };

    braintree();
  }]);
