'use strict';

App.controller('LoginsController', ['$scope', '$rootScope', '$modalInstance', 'AuthService', 'AUTH_EVENTS',
  function ($scope, $rootScope, $modalInstance, AuthService, AUTH_EVENTS) {
    $scope.credentials = {
      username: '',
      password: ''
    };

    $scope.modalClose = function () {
      $modalInstance.close();
    };

    $scope.loginForm = function (credentials) {
      AuthService.login(credentials).success(function (data) {
        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
        $scope.modalClose();
        $rootScope.setCurrentUserData(data);
      }).error(function (data) {
        $rootScope.loginError = 'Invalid Username or Password';
        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
      });
    };
  }]);