'use strict';

App.controller('ApplicationController', ['$rootScope', 'AuthService', '$cookies', 'AUTH_EVENTS', '$location', '$routeParams',
  function ($rootScope, AuthService, $cookies, AUTH_EVENTS, $location, $routeParams) {
  var loggedOutTemplate = 'views/landing.html';
  var loggedInTemplate = 'views/dashboard.html';
  var authToken = $cookies._nurseLola;

  $rootScope.mainTemplateUrl = loggedOutTemplate;
  
  $rootScope.setCurrentUserData = function (data) {
    $rootScope.currentUser = data.user;
    $rootScope.organizations = data.organizations;
    $rootScope.mainTemplateUrl = loggedInTemplate;
    $location.path('/');
    $routeParams = {};
  };

  $rootScope.setProfilePicture = function (avatar) {
    $rootScope.currentUser.avatar = avatar;
  };

  if(authToken) {
    AuthService.fetchCurrentUser(authToken)
      .success(function(data){
        $rootScope.setCurrentUserData(data);
        $rootScope.organizations = data.organizations;
        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
      });
  } else {
    $rootScope.currentUser = null;
  }
  
  $rootScope.isAuthorized = AuthService.isAuthorized;

  $rootScope.$on(AUTH_EVENTS.logoutSuccess, function () {
    $rootScope.currentUser = null;
    $rootScope.organizations = null;
    $rootScope.mainTemplateUrl = loggedOutTemplate;
  });

  $rootScope.$on('$locationChangeStart', function () {
    var forgotPage = 'forgot-page';
    
    var urlToPageClass = {
      '/': 'main-page',
      '/users/new': 'registrations-page',
      '/sessions/new': 'sessions-page',
      '/dashboard': 'main-page',
      '/forgot_password': forgotPage,
      '/forgot_password_reset': forgotPage,
      '/forgot_username': forgotPage,
      '/forgot_username_reset': forgotPage,
    };

    $rootScope.pageClass = urlToPageClass[$location.path()];
  });
}]);
