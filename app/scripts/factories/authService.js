'use strict';

App.factory('AuthService', ['$http', 'Session', 'Server', '$rootScope', 
    function ($http, Session, Server, $rootScope) {
    
    function getCurrentUser (data) {
      $rootScope.currentUser = data.user;
      return $rootScope.currentUser;
    }

    function createSession (data) {
      Session.create(data.user.auth_token, data.user.id);
      return getCurrentUser(data);
    }

    var authService = {};
   
    authService.login = function (credentials) {
      return $http.post(Server + '/users/login', {
         user: credentials 
        })
      .success(createSession);
    };

    authService.logout = function () {
      Session.destroy();
    };

    authService.fetchCurrentUser = function (authToken) {
      return $http.get(Server + '/dashboard?auth_token=' + authToken)
        .success(getCurrentUser);
    };

    authService.isAuthenticated = function () {
      return !!Session.userId;
    };
   
    authService.isAuthorized = function (authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }
      return (authService.isAuthenticated() &&
        authorizedRoles.indexOf(Session.userRole) !== -1);
    };
   
    return authService;
}]);